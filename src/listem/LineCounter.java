package listem;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

public class LineCounter extends Searcher implements ILineCounter{

//	String linePattern;
//	String filePattern;
//	boolean recursive;
	Map<File, Integer> result;
	
	public LineCounter()
	{

	}
	
	@Override
	public Map<File, Integer> countLines(File directory, String filePattern, boolean recursive) {
		// TODO Auto-generated method stub
		
		this.filePattern = filePattern;
		linePattern = "*";
		this.recursive = recursive;
		result = new HashMap<File, Integer>();
		
		processDir(directory);
		
		return result;
	}

	@Override
	void processLine(File file, String s)
	{
		
		if(result.get(file) != null)
		{
			Integer temp = result.get(file);
			temp++;
			result.put(file,  result.get(file) + 1);	
		}
		else
		{
			result.put(file,  new Integer(1));
			System.out.println("pause");
		}
	}

}
