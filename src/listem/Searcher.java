package listem;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public abstract class Searcher {

	protected boolean recursive;
	protected String filePattern;
	protected String linePattern;
//	Map<Object, Object> result;
	
	
	public void processDir(File file)
	{
		
		for(File f : file.listFiles())
		{
			if(f.isDirectory())
			{
				if(recursive)
				{
					processDir(f);
				}
			}
			else
			{
				processFile(f);
			}
		}
		
	}
	
	public void processFile(File file)
	{
		Pattern p = Pattern.compile(filePattern);
		Matcher m = p.matcher(file.getName());
		if(m.matches()) // Pattern.matches(filePattern, file.getName()) ----- file.getName().matches(filePattern)
		{
			try 
			{
				Scanner scanner = new Scanner(new BufferedReader(new FileReader(file)));
				
				while(scanner.hasNextLine())
				{
					String line = scanner.nextLine();
					processLine(file, line);
				}
			}
			catch (FileNotFoundException e) 
			{	
				e.printStackTrace();
			}
		}
	}
	
	abstract void processLine(File file, String s);
	
}
