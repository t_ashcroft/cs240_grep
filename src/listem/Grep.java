package listem;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Grep extends Searcher implements IGrep {

//	private String filePattern;
//	private String linePattern;
	private Map<File, List<String>> result;
//	boolean recursive;
	
	public Grep()
	{
	}
	
	@Override
	public Map<File, List<String>> grep(File directory, String filePattern, String substringPattern,
			boolean recursive) {
		
		this.filePattern = filePattern;
		linePattern = substringPattern;
		this.recursive = recursive;
		result = new HashMap<File, List<String>>();
		
		processDir(directory);		
		
		return result;
	}

	@Override
	void processLine(File file, String s) {
		Pattern p = Pattern.compile(linePattern);
		Matcher m = p.matcher(s);
		if(m.find()) // Pattern.matches(linePattern, s); ---- s.matches(linePattern)
		{			
			if(result.containsKey(file))
			{
				result.get(file).add(s);
			}
			else
			{
				result.put(file, new ArrayList<String>());
				result.get(file).add(s);
			}
		}
		
	}

}
